
#Important to put HIGHEST first
GER_mefo_bills_level_up = {
	if = {
		limit = { 
			has_dynamic_modifier = {
				modifier = MEFO_bills_modifier_spirit
				scope = GER
			}
		}
		add_to_variable = { ROOT.mefoppmodifier = 0.05 } 
	}
}

#Important to put HIGHEST first
GER_remove_mefo_bills = {
	if = {
		limit = { 
			has_dynamic_modifier = {
				modifier = MEFO_bills_modifier_spirit
				scope = GER
			}
		}
		set_country_flag = GER_mefo_expired
	}
}

# Like the level up effect except it runs in reverse
# For use when annexing the gold reserves of Austria, Czechoslovakia and Yugoslavia


give_SOV_armor_research_bonuses = {
	add_tech_bonus = {
		name = med_armor_bonus
		ahead_reduction = 2
		category = cat_medium_armor
	}
	add_tech_bonus = {
		name = armor_bonus
		bonus = 1.0
		uses = 1
		category = armor
	}
}