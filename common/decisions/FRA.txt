FRA_vichy_france = {

	FRA_scuttle_the_fleet = {

		icon = generic_ignite_civil_war

		available = {
			has_government = fascism
			NOT = {
				GER = { divisions_in_state = { state = 21 size > 0 } }
			}
			has_country_flag = {
				flag = case_anton
				days > 2
			}
		}

		cost = 0

		ai_will_do = {
			factor = 200
		}

		visible = {
			has_government = fascism
			has_country_flag = case_anton
		}

		complete_effect = {
			set_country_flag = scuttled_fleet
			destroy_ships = {
				type = ship_hull_light
				count = all
			}
			destroy_ships = {
				type = ship_hull_cruiser
				count = all
			}
			destroy_ships = {
				type = ship_hull_heavy
				count = all
			}
			destroy_ships = {
				type = ship_hull_carrier 
				count = all
			}
			destroy_ships = {
				type = ship_hull_submarine
				count = all
			}
		}
	}

	FRA_case_anton_mission = {

		icon = generic_tank

		available = {
			is_subject = no
			has_war_with = GER
		}

		days_mission_timeout = 7
		fire_only_once = yes

		activation = {
			original_tag = FRA
			has_government = fascism
			OR = {
				is_subject_of = GER
				has_focus_tree = vichy_french_focus
			}
			has_country_flag = case_anton
		}

		complete_effect = {
			custom_effect_tooltip = FRA_case_anton_mission_tt
		}

		is_good = no

		timeout_effect = {
			transfer_navy = { target = GER }
			GER = { annex_country = { target = ROOT } }
			if = {
				limit = {
					NOT = { has_country_flag = scuttled_fleet }
				}
				GER = { news_event = { id = wtt_news.23 hours = 6 } }
			}
			if = {
				limit = {
					has_country_flag = scuttled_fleet
				}
				GER = { news_event = { id = wtt_news.24 hours = 6 } }
			}
			custom_effect_tooltip = GAME_OVER_TT
		}
	}
	FRA_demand_unification_with_vichy = {
		available = {
			VIC = {
				exists = yes
				NOT = {
					has_war_with = ROOT
				}
			}
			controls_state = 16 #Paris
		}

		visible = {
			NOT = {
				tag = VIC
			}
			country_exists = VIC
		}
		ai_will_do = {
			factor = 100
		}
		cost = 0
		fire_only_once = yes
		complete_effect = {
			random_other_country = {
				limit = {
					tag = VIC
				}
				country_event = lar_france_vichy_reunification.1
			}
		}
	}
	FRA_failsafe_join_axis = {
		available = {
			tag = VIC
			NOT = {
				has_war_with = GER
				is_in_faction_with = GER
			}
			date > 1942.6.1
			FRA = {
				any_controlled_state = {
					NOT = { #algeria
						state = 459
						state = 460
						state = 513
					}
					is_core_of = FRA
				}
			}
		}
		visible = {
			is_ai = yes #if you are not a member of the dev team - avert thy eyes, traveller, for thou hath gazed upon my shame
		}
		ai_will_do = {
			factor = 10
		}
		cost = 0
		fire_only_once = yes
		complete_effect = {
			set_rule = { can_join_factions = yes }
			GER = { add_to_faction = ROOT }
		}
	}
}
category_test_decisions = {
	test_free_france = {
		allowed = { original_tag = FRA }
		cost = 0
		ai_will_do = {
			factor = 0
		}
		complete_effect = {
			load_focus_tree = free_french_focus
		}
	}
	test_vichy_france = {
		allowed = { original_tag = FRA }
		cost = 0
		ai_will_do = {
			factor = 0
		}
		complete_effect = {
			create_dynamic_country = {
				original_tag = FRA
				save_event_target_as = france_vichy
				every_state = {
					limit = {
						is_controlled_by = ROOT
						is_on_continent = africa
					}
					event_target:france_vichy = { transfer_state = PREV }
				}
				every_state = {
					limit = {
						is_controlled_by = ROOT
						OR = {
							state = 735
							state = 21
							state = 32
							state = 20
							state = 26
							state = 22
							state = 31
							state = 25
							state = 33
						}
					}
					add_core_of = PREV
					event_target:france_vichy = { transfer_state = PREV }
				}
				every_state = {
					limit = {
						is_controlled_by = ROOT
						is_core_of = ROOT
						NOT = {
							is_controlled_by = event_target:france_vichy
						}
					}
					add_core_of = PREV
				}
				set_capital = 26
				set_politics = {
					ruling_party = fascism
					elections_allowed = no
				}
				set_popularities = {
					democratic = 0
					neutrality = 20
					fascism = 80
					communism = 0
				}
				set_cosmetic_tag = FRA_VICHY
				create_country_leader = {
					name = "Philippe Pétain"
					desc = "POLITICS_PHILIPPE_PÉTAIN_DESC"
					picture = "Portrait_France_Philippe_Petain.dds"
					expire = "1965.1.1"
					ideology = fascism_ideology
					traits = {
						
					}
				}
				add_ideas = FRA_occupation_costs_4
				load_focus_tree = vichy_french_focus
				FRA = {
					transfer_units_fraction = {
						target = event_target:france_vichy
						size = 0.9
						stockpile_ratio = 0.7
						army_ratio = 0.5
						navy_ratio = 0.85
						air_ratio = 1.0
					}
				}
				change_tag_from = FRA
			}
		}
	}
}

FRA_weapons_purchases_category = {
	FRA_order_weapons_in_USA = {
	icon = generic_prepare_civil_war
		available = {
			
		}
		visible = {
			USA_can_sell_weapons_trigger = yes
		}
		days_remove = 60
		cost = 50
		ai_will_do = {
			factor = 10
			modifier = {
				has_equipment = {
					infantry_equipment < 1
				}
				factor = 50
			}
			modifier = {
				has_manpower < 10000 
				factor = 0 #need to up conscription laws
			}
		}
		complete_effect = {
			USA = {
				add_offsite_building = { type = industrial_complex level = 2 }
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = 2500
			}
			USA = {
				add_offsite_building = { type = industrial_complex level = -2 }
			}
		}
	}
	FRA_order_artillery_in_USA = {
		icon = ger_military_buildup
		available = {
			
		}
		visible = {
			USA_can_sell_weapons_trigger = yes
		}
		days_remove = 60
		cost = 75
		ai_will_do = {
			factor = 5
			modifier = {
				has_equipment = {
					artillery_equipment < 1
				}
				factor = 50
			}
			modifier = {
				has_manpower < 10000 
				factor = 0 #need to up conscription laws
			}
		}
		complete_effect = {
			USA = {
				add_offsite_building = { type = industrial_complex level = 3 }
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_equipment
				amount = 75
			}
			USA = {
				add_offsite_building = { type = industrial_complex level = -3 }
			}
		}
	}
	FRA_order_tanks_in_USA = {
		icon = generic_air
		available = {
			
		}
		visible = {
			USA_can_sell_weapons_trigger = yes
		}
		days_remove = 60
		cost = 100
		ai_will_do = {
			factor = 1
			modifier = {
				has_equipment = {
					light_tank_equipment < 1
				}
				factor = 5
			}
			modifier = {
				has_manpower < 10000 
				factor = 0 #need to up conscription laws
			}
		}
		modifier = {
		}
		complete_effect = {
			USA = {
				add_offsite_building = { type = industrial_complex level = 5 }
			}
		}
		remove_effect = {
			if = {
				limit = {
					USA = { 
						has_tech = basic_light_tank 
						NOT = { 
							OR = {
								has_tech = basic_medium_tank 
								has_tech = improved_medium_tank #needed because basic can be bypassed
							}
						}
					}
				}
				add_equipment_to_stockpile = {
					type = light_tank_equipment
					amount = 50
				}
			}
			else_if = {
				limit = {
					USA = {
						OR = {
							has_tech = basic_medium_tank 
							has_tech = improved_medium_tank #needed because basic can be bypassed
						}
					}
				}
				add_equipment_to_stockpile = {
					type = medium_tank_equipment
					amount = 50
				}
			}
			USA = {
				add_offsite_building = { type = industrial_complex level = -5 }
			}
		}
	}
	FRA_order_fighters_in_USA = {
		icon = generic_air
		available = {
			
		}
		visible = {
			USA_can_sell_weapons_trigger = yes
		}
		days_remove = 60
		cost = 100
		ai_will_do = {
			factor = 1
			modifier = {
				has_equipment = {
					fighter_equipment < 1
				}
				factor = 5
			}
			modifier = {
				has_manpower < 10000 
				factor = 0 #need to up conscription laws
			}
		}
		modifier = {
		}
		complete_effect = {
			USA = {
				add_offsite_building = { type = industrial_complex level = 5 }
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = fighter_equipment
				amount = 50
			}
			USA = {
				add_offsite_building = { type = industrial_complex level = -5 }
			}
		}
	}
	FRA_order_bombers_in_USA = {
		icon = generic_air
		available = {
			
		}
		visible = {
			USA_can_sell_weapons_trigger = yes
		}
		days_remove = 60
		cost = 100
		ai_will_do = {
			factor = 1
			modifier = {
				has_equipment = {
					tac_bomber_equipment < 1
				}
				factor = 5
			}
			modifier = {
				has_manpower < 10000 
				factor = 0 #need to up conscription laws
			}
		}
		modifier = {
			
		}
		complete_effect = {
			USA = {
				add_offsite_building = { type = industrial_complex level = 5 }
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = tac_bomber_equipment
				amount = 25
			}
			USA = {
				add_offsite_building = { type = industrial_complex level = -5 }
			}
		}
	}
}

economy_decisions = {

	FRA_revoke_the_matignon_agreements = {
		visible = { has_idea = FRA_matignon_agreements }

		cost = 50

		ai_will_do = {
			factor = 10
			modifier = {
				has_war = no
				factor = 0
			}
		}

		complete_effect = {
			remove_ideas = FRA_matignon_agreements
			if = {
				limit = {
					communism > 0.25
				}
				country_event = lar_france_political_violence.2
			}
		}
	}

	FRA_reorganize_aviation_industry_north = {
		available = {  }

		visible = { has_completed_focus = FRA_reorganize_the_aviation_industry }

		remove_effect = { 
			set_country_flag = FRA_SNCAN_formed
			random_owned_state = {
				limit = { 
					OR = {
						state = 29 
						state = 785 
						state = 15 
						state = 16
					}
					is_fully_controlled_by = ROOT 
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = yes
					}
				}
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}
		days_remove = 120
		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 5
		}
		
	}
	FRA_reorganize_aviation_industry_west = {
		available = {  }

		visible = { has_completed_focus = FRA_reorganize_the_aviation_industry }

		remove_effect = { 
			set_country_flag = FRA_SNCAO_formed
			random_owned_state = {
				limit = { 
					OR = {
						state = 30
						state = 14
						state = 23
					}
					is_fully_controlled_by = ROOT 
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = yes
					}
				}
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}
		days_remove = 120
		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 5
		}
	}
	FRA_reorganize_aviation_industry_center = {
		available = {  }

		visible = { has_completed_focus = FRA_reorganize_the_aviation_industry }

		remove_effect = { 
			set_country_flag = FRA_SNCAC_formed
			random_owned_state = {
				limit = { 
					OR = {
						state = 24
						state = 33
						state = 27
						state = 26
						state = 25
					}
					is_fully_controlled_by = ROOT 
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = yes
					}
				}
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}
		days_remove = 120
		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 5
		}
	}
	FRA_reorganize_aviation_industry_south_east = {
		available = {  }

		visible = { has_completed_focus = FRA_reorganize_the_aviation_industry }

		remove_effect = { 
			set_country_flag = FRA_SNCASE_formed
			random_owned_state = {
				limit = { 
					OR = {
						state = 735
						state = 32
						state = 20
						state = 21
					}
					is_fully_controlled_by = ROOT 
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = yes
					}
				}
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}
		days_remove = 120
		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 5
		}
	}
	FRA_reorganize_aviation_industry_south_west = {
		available = {  }

		visible = { has_completed_focus = FRA_reorganize_the_aviation_industry }

		remove_effect = { 
			set_country_flag = FRA_SNCASO_formed
			random_owned_state = {
				limit = { 
					OR = {
						state = 22
						state = 31
						state = 19
						state = 25
					}
					is_fully_controlled_by = ROOT 
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = yes
					}
				}
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}
		days_remove = 120
		cost = 50
		fire_only_once = yes
		ai_will_do = {
			factor = 5
		}
	}
}
political_actions = {

}
VIC_concessions_to_the_germans = {
	VIC_basing_rights = {
		available = {

		}
		visible = {
			has_any_occupation_cost_trigger = yes
		}

		cost = 25

		fire_only_once = yes
		ai_will_do = {
			factor = 1
			modifier = {
				has_idea = FRA_occupation_costs_4
				factor = 5
			}
			modifier = {
				has_idea = FRA_occupation_costs_3
				factor = 2
			}
			modifier = {
				has_idea = FRA_occupation_costs_2
				factor = 1.5
			}
		}
		complete_effect = {
			give_military_access = GER
			reduce_occupation_cost_effect = yes
		}
	}

	VIC_produce_aircraft_parts = {
		available = {}

		visible = {
			has_any_occupation_cost_trigger = yes
		}

		cost = 25

		fire_only_once = yes
		ai_will_do = {
			factor = 1
			modifier = {
				has_idea = FRA_occupation_costs_4
				factor = 5
			}
			modifier = {
				has_idea = FRA_occupation_costs_3
				factor = 2
			}
			modifier = {
				has_idea = FRA_occupation_costs_2
				factor = 1.5
			}
		}
		complete_effect = {
			add_ideas = VIC_produce_aircraft_parts
			GER = { add_ideas = VIC_aircraft_parts }
			reduce_occupation_cost_effect = yes
		}
	}
	VIC_send_guest_workers = {
		available = {
			
		}
		visible = { has_any_occupation_cost_trigger = yes }
		cancel_trigger = {
			not = { has_country_flag = VIC_send_guest_workers }
		}
		cost = 25
		ai_will_do = {
			factor = 1
			modifier = {
				has_idea = FRA_occupation_costs_4
				factor = 5
			}
			modifier = {
				has_idea = FRA_occupation_costs_3
				factor = 2
			}
			modifier = {
				has_idea = FRA_occupation_costs_2
				factor = 1.5
			}
		}
		fire_only_once = yes
		days_remove = -1
		modifier = {
			conscription_factor = -0.25
		}
		complete_effect = {
			GER = { add_ideas = VIC_guest_workers }
			reduce_occupation_cost_effect = yes
			set_country_flag = VIC_send_guest_workers
		}
	}
	VIC_recall_guest_workers = {
		available = {
			has_country_flag = VIC_send_guest_workers
		}
		visible = { 
		 	has_country_flag = VIC_send_guest_workers
		}
		cost = 0
		ai_will_do = {
			factor = 0
			modifier = {
				controls_state = 16
				add = 10
			}
		}
		fire_only_once = yes
		complete_effect = {
			GER = { remove_ideas = VIC_guest_workers }
			if = {
				limit = {
					NOT = {
						controls_state = 16
					}
				}
				increase_occupation_cost_effect = yes
			}
			if = {
				limit = {
					controls_state = 16
					has_idea = FRA_mandatory_work_service
				}
				remove_ideas = FRA_mandatory_work_service
			}
			clr_country_flag = VIC_send_guest_workers
		}
	}	
}
FRA_intervention_in_overseas_territories = {
	
	FRA_prepare_coup_in_north_africa = {
		available = { 
			NOT = {
				FRA_controls_north_africa = yes
			}
			has_intelligence_agency = yes
		} 

		visible = { 
			has_completed_focus = FRA_intervention_in_north_africa 
		}

		complete_effect = { 
			set_country_flag = FRA_coup_in_north_africa_flag
			custom_effect_tooltip = FRA_prepare_coup_tt
			
			
		}
		fire_only_once = yes
		cost = 25
		ai_will_do = {
			factor = 0 #at least until we know the AI can handle it
		}
	}
	
	FRA_prepare_coup_in_syria = {
		available = { 
			NOT = {
				FRA_controls_syria = yes
			}
			has_intelligence_agency = yes
		} 

		visible = { 
			has_completed_focus = FRA_intervention_in_syria 
			NOT = {
				FRA_controls_syria = yes
			}
		}

		complete_effect = { 
			custom_effect_tooltip = FRA_prepare_coup_tt
			set_country_flag = FRA_coup_in_syria_flag
		}
		
		fire_only_once = yes
		cost = 25
		ai_will_do = {
			factor = 0
		}
	}
	FRA_invasion_in_syria = {
		available = { 
			NOT = {
				FRA_controls_syria = yes
			}
			any_state = {
				OR = {
					state = 680
					state = 677
					state = 554
					state = 553
				}
				CONTROLLER = {
					NOT = {
						has_war_with = ROOT
					}
				}
				any_neighbor_state = {
					CONTROLLER = {
						OR = {
							tag = ROOT
							is_in_faction_with = ROOT
						}
					}
				}
			} 
		} 

		visible = { has_completed_focus = FRA_intervention_in_syria }

		complete_effect = { 
			random_state = { #find a neighbouring state of Syria that is controlled by an ally or ourselves
				limit = {
					OR = {
						state = 680
						state = 677
						state = 554
						state = 553
					}
					not = { is_controlled_by = ROOT }
					CONTROLLER = {
						NOT = { has_war_with = ROOT }
					}
					any_neighbor_state = {
						CONTROLLER = {
							OR = {
								tag = ROOT
								is_in_faction_with = ROOT
							}
						}
					}
				}
				save_event_target_as = FRA_defender_state
				random_neighbor_state = {
					limit = {
						CONTROLLER = {
							OR = {
								tag = ROOT
								is_in_faction_with = ROOT
							}
						}
					}
					save_event_target_as = FRA_attacker_state
				}
			}
			event_target:FRA_attacker_state = {
				CONTROLLER = {
					division_template = {
						name = "Syria Intervention Force" 	
						division_names_group = FRA_INF_01
						is_locked = yes
						regiments = {
							infantry = { x = 0 y = 0 }		
							infantry = { x = 0 y = 1 }		
							infantry = { x = 0 y = 2 }

							infantry = { x = 1 y = 0 }		
							infantry = { x = 1 y = 1 }
							infantry = { x = 1 y = 2 }		
						}
						support = {
							engineer = { x = 0 y = 0 }
							artillery = {x = 0 y = 1 }
						}
					}
					event_target:FRA_attacker_state = { #set up attacker unit(s)
						create_unit = {
							division = "name = \"Gentforce 1\" division_template = \"Syria Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
						create_unit = {
							division = "name = \"Gentforce 2\" division_template = \"Syria Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
						create_unit = {
							division = "name = \"Gentforce 3\" division_template = \"Syria Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
					}
				}
			}
			event_target:FRA_defender_state = { #set up defender unit(s)
				CONTROLLER = {
					division_template = {
						name = "Syria Defense Force" 	
						division_names_group = FRA_INF_01
						is_locked = yes
						regiments = {
							infantry = { x = 0 y = 0 }		
							infantry = { x = 0 y = 1 }		
							infantry = { x = 0 y = 2 }

							infantry = { x = 1 y = 0 }		
							infantry = { x = 1 y = 1 }
							infantry = { x = 1 y = 2 }		
						}
						support = {
							engineer = { x = 0 y = 0 }
							artillery = {x = 0 y = 1 }
						}
					}
					event_target:FRA_defender_state = { #set up defenders
						create_unit = {
							division = "name = \"Syria Defense Force 1\" division_template = \"Syria Defense Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
					}
				}
			}
			event_target:FRA_attacker_state = {
				CONTROLLER = {
					start_border_war = {
						change_state_after_war = no
						attacker = {
							state = event_target:FRA_attacker_state
							num_provinces = 4
							on_win = FRA_syria_intervention.1
							on_lose = FRA_syria_intervention.2
							on_cancel = FRA_syria_intervention.3
						}
						defender = {
							state = event_target:FRA_defender_state
							num_provinces = 4
							on_win = FRA_syria_intervention.2
							on_lose = FRA_syria_intervention.1
							on_cancel = FRA_syria_intervention.3
						}
					}
				}
			}

		}
		fire_only_once = yes
		cost = 25
		ai_will_do = {
			factor = 5
		}
		
	}
	
	FRA_prepare_coup_in_indochina = {
		available = { 
			NOT = {
				FRA_controls_indochina = yes
			}
			has_intelligence_agency = yes
		} 

		visible = { 
			has_completed_focus = FRA_intervention_in_indochina 
		}

		complete_effect = { 
			custom_effect_tooltip = FRA_prepare_coup_tt
			set_country_flag = FRA_coup_in_indochina_flag
		}
		fire_only_once = yes
		cost = 25
		ai_will_do = {
			factor = 0
		}
	}
	FRA_invasion_in_indochina = {
		available = { 
			NOT = {
				FRA_controls_indochina = yes
			}
			any_state = {
				OR = {
					state = 671
					state = 670
					state = 286
					state = 741
				}
				CONTROLLER = {
					NOT = {
						has_war_with = ROOT
					}
				}
				any_neighbor_state = {
					CONTROLLER = {
						OR = {
							tag = ROOT
							is_in_faction_with = ROOT
						}
					}
				}
			} 
		} 

		visible = { has_completed_focus = FRA_intervention_in_indochina }

		complete_effect = { 
			random_state = { #find a neighbouring state of Syria that is controlled by an ally or ourselves
				limit = {
					OR = {
						state = 671
						state = 670
						state = 286
						state = 741
					}
					not = { is_controlled_by = ROOT }
					CONTROLLER = {
						NOT = { has_war_with = ROOT }
					}
					any_neighbor_state = {
						CONTROLLER = {
							OR = {
								tag = ROOT
								is_in_faction_with = ROOT
							}
						}
					}
				}
				save_event_target_as = FRA_defender_state
				random_neighbor_state = {
					limit = {
						CONTROLLER = {
							OR = {
								tag = ROOT
								is_in_faction_with = ROOT
							}
						}
					}
					save_event_target_as = FRA_attacker_state
				}
				event_target:FRA_attacker_state = {
				CONTROLLER = {
					division_template = {
						name = "Indochina Intervention Force" 	
						division_names_group = FRA_INF_01
						is_locked = yes
						regiments = {
							infantry = { x = 0 y = 0 }		
							infantry = { x = 0 y = 1 }		
							infantry = { x = 0 y = 2 }

							infantry = { x = 1 y = 0 }		
							infantry = { x = 1 y = 1 }
							infantry = { x = 1 y = 2 }		
						}
						support = {
							engineer = { x = 0 y = 0 }
							artillery = {x = 0 y = 1 }
						}
					}
					event_target:FRA_attacker_state = { #set up attacker unit(s)
						create_unit = {
							division = "name = \"Indochina Force 1\" division_template = \"Indochina Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
						create_unit = {
							division = "name = \"Indochina Force 2\" division_template = \"Indochina Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
						create_unit = {
							division = "name = \"Indochina Force 3\" division_template = \"Indochina Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
					}
				}
			}
			event_target:FRA_defender_state = { #set up defender unit(s)
				CONTROLLER = {
					division_template = {
						name = "Indochina Defense Force" 	
						division_names_group = FRA_INF_01
						is_locked = yes
						regiments = {
							infantry = { x = 0 y = 0 }		
							infantry = { x = 0 y = 1 }		
							infantry = { x = 0 y = 2 }

							infantry = { x = 1 y = 0 }		
							infantry = { x = 1 y = 1 }
							infantry = { x = 1 y = 2 }		
						}
						support = {
							engineer = { x = 0 y = 0 }
							artillery = {x = 0 y = 1 }
						}
					}
					event_target:FRA_defender_state = { #set up defenders
						create_unit = {
							division = "name = \"Indochina Defense Force 1\" division_template = \"Indochina Defense Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
					}
				}
			}
			}
			event_target:FRA_attacker_state = {
				CONTROLLER = {
					start_border_war = {
						change_state_after_war = no
						attacker = {
							state = event_target:FRA_attacker_state
							num_provinces = 4
							on_win = FRA_indochina_intervention.1
							on_lose = FRA_indochina_intervention.2
							on_cancel = FRA_indochina_intervention.3
						}
						defender = {
							state = event_target:FRA_defender_state
							num_provinces = 4
							on_win = FRA_indochina_intervention.2
							on_lose = FRA_indochina_intervention.1
							on_cancel = FRA_indochina_intervention.3
						}
					}
				}
			}

		}
		fire_only_once = yes
		ai_will_do = {
			factor = 0
		}
		cost = 25
	}
	
	FRA_prepare_coup_in_central_africa = {
		available = { 
			NOT = {
				FRA_controls_central_africa = yes
			}
			has_intelligence_agency = yes
		} 

		visible = { 
			has_completed_focus = FRA_intervention_in_central_africa 
		}

		complete_effect = {
			custom_effect_tooltip = FRA_prepare_coup_tt
			set_country_flag = FRA_coup_in_central_africa_flag
		}

		fire_only_once = yes
		cost = 25
		ai_will_do = {
			factor = 0
		}
	}
	FRA_invasion_in_central_africa = {
		available = { 
			NOT = {
				FRA_controls_central_africa = yes
			}
			any_state = {
				OR = {
					state = 772
					state = 539
					state = 773
					state = 660
					state = 774
				}
				CONTROLLER = {
					NOT = { has_war_with = ROOT }
				}
				any_neighbor_state = {
					CONTROLLER = {
						OR = {
							tag = ROOT
							is_in_faction_with = ROOT
						}
					}
				}
			} 
		} 

		visible = { has_completed_focus = FRA_intervention_in_central_africa }

		complete_effect = { 
			random_state = { #find a neighbouring state of Syria that is controlled by an ally or ourselves
				limit = {
					OR = {
						state = 772
						state = 539
						state = 773
						state = 660
						state = 774
					}
					not = { is_controlled_by = ROOT }
					CONTROLLER = {
						NOT = { has_war_with = ROOT }
					}
					any_neighbor_state = {
						CONTROLLER = {
							OR = {
								tag = ROOT
								is_in_faction_with = ROOT
							}
						}
					}
				}
				save_event_target_as = FRA_defender_state
				random_neighbor_state = {
					limit = {
						CONTROLLER = {
							OR = {
								tag = ROOT
								is_in_faction_with = ROOT
							}
						}
					}
					save_event_target_as = FRA_attacker_state
				}
			}
			event_target:FRA_attacker_state = {
				CONTROLLER = {
					division_template = {
						name = "Central Africa Intervention Force" 	
						division_names_group = FRA_INF_01
						is_locked = yes
						regiments = {
							infantry = { x = 0 y = 0 }		
							infantry = { x = 0 y = 1 }		
							infantry = { x = 0 y = 2 }

							infantry = { x = 1 y = 0 }		
							infantry = { x = 1 y = 1 }
							infantry = { x = 1 y = 2 }		
						}
						support = {
							engineer = { x = 0 y = 0 }
							artillery = { x = 0 y = 1 }
							recon = { x = 0 y = 2 }
						}
					}
					event_target:FRA_attacker_state = { #set up attacker unit(s)
						create_unit = {
							division = "name = \"Central Africa Force 1\" division_template = \"Central Africa Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
						create_unit = {
							division = "name = \"Central Africa Force 2\" division_template = \"Central Africa Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
						create_unit = {
							division = "name = \"Central Africa Force 3\" division_template = \"Central Africa Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
						create_unit = {
							division = "name = \"Central Africa Force 4\" division_template = \"Central Africa Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
						create_unit = {
							division = "name = \"Central Africa Force 5\" division_template = \"Central Africa Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
					}
				}
			}
			event_target:FRA_defender_state = { #set up defender unit(s)
				CONTROLLER = {
					division_template = {
						name = "Central Africa Defense Force" 	
						division_names_group = FRA_INF_01
						is_locked = yes
						regiments = {
							infantry = { x = 0 y = 0 }		
							infantry = { x = 0 y = 1 }		
							infantry = { x = 0 y = 2 }

							infantry = { x = 1 y = 0 }		
							infantry = { x = 1 y = 1 }
							infantry = { x = 1 y = 2 }		
						}
						support = {
							engineer = { x = 0 y = 0 }
							artillery = {x = 0 y = 1 }
						}
					}
					event_target:FRA_defender_state = { #set up defenders
						create_unit = {
							division = "name = \"Central Africa Defense Force 1\" division_template = \"Central Africa Defense Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
					}
				}
			}
			event_target:FRA_attacker_state = {
				CONTROLLER = {
					start_border_war = {
						change_state_after_war = no
						attacker = {
							state = event_target:FRA_attacker_state
							num_provinces = 4
							on_win = FRA_central_africa_intervention.1
							on_lose = FRA_central_africa_intervention.2
							on_cancel = FRA_central_africa_intervention.3
						}
						defender = {
							state = event_target:FRA_defender_state
							num_provinces = 4
							on_win = FRA_central_africa_intervention.2
							on_lose = FRA_central_africa_intervention.1
							on_cancel = FRA_central_africa_intervention.3
						}
					}
				}
			}
		}
		fire_only_once = yes
		cost = 25
		ai_will_do = {
			factor = 0
		}
	}
	
	FRA_prepare_coup_in_west_africa = {
		available = { 
			NOT = {
				FRA_controls_west_africa = yes
			}
			has_intelligence_agency = yes
		} 

		visible = { 
			has_completed_focus = FRA_intervention_in_west_africa 
		}

		complete_effect = { 
			custom_effect_tooltip = FRA_prepare_coup_tt
			set_country_flag = FRA_coup_in_west_africa_flag
						
		}
		fire_only_once = yes
		cost = 25
		ai_will_do = {
			factor = 0
		}
	}
	FRA_invasion_in_west_africa = {
		available = { 
			NOT = {
				FRA_controls_west_africa = yes
			}
			any_state = {
				OR = {
						state = 557
						state = 272
						state = 556
						state = 780
						state = 779
						state = 778
						state = 781
						state = 776
				}
				CONTROLLER = {
					NOT = { has_war_with = ROOT }
				}
				any_neighbor_state = {
					CONTROLLER = {
						OR = {
							tag = ROOT
							is_in_faction_with = ROOT
						}
					}
				}
			} 
		} 

		visible = { has_completed_focus = FRA_intervention_in_west_africa }

		complete_effect = { 
			random_state = { #find a neighbouring state of Syria that is controlled by an ally or ourselves
				limit = {
					OR = {
						state = 557
						state = 272
						state = 556
						state = 780
						state = 779
						state = 778
						state = 781
						state = 776
					}
					not = { is_controlled_by = ROOT }
					CONTROLLER = {
						NOT = { has_war_with = ROOT }
					}
					any_neighbor_state = {
						CONTROLLER = {
							OR = {
								tag = ROOT
								is_in_faction_with = ROOT
							}
						}
					}
				}
				save_event_target_as = FRA_defender_state
				random_neighbor_state = {
					limit = {
						CONTROLLER = {
							OR = {
								tag = ROOT
								is_in_faction_with = ROOT
							}
						}
					}
					save_event_target_as = FRA_attacker_state
				}
			}
			event_target:FRA_attacker_state = {
				CONTROLLER = {
					division_template = {
						name = "West Africa Intervention Force" 	
						division_names_group = FRA_INF_01
						is_locked = yes
						regiments = {
							infantry = { x = 0 y = 0 }		
							infantry = { x = 0 y = 1 }		
							infantry = { x = 0 y = 2 }

							infantry = { x = 1 y = 0 }		
							infantry = { x = 1 y = 1 }
							infantry = { x = 1 y = 2 }		
						}
						support = {
							engineer = { x = 0 y = 0 }
							artillery = {x = 0 y = 1 }
						}
					}
					event_target:FRA_attacker_state = { #set up attacker unit(s)
						create_unit = {
							division = "name = \"West Africa Force 1\" division_template = \"West Africa Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
						create_unit = {
							division = "name = \"West Africa Force 2\" division_template = \"West Africa Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
						create_unit = {
							division = "name = \"West Africa Force 3\" division_template = \"West Africa Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
						create_unit = {
							division = "name = \"West Africa Force 4\" division_template = \"West Africa Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
						create_unit = {
							division = "name = \"West Africa Force 5\" division_template = \"West Africa Intervention Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
					}
				}
			}
			event_target:FRA_defender_state = { #set up defender unit(s)
				CONTROLLER = {
					division_template = {
						name = "West Africa Defense Force" 	
						division_names_group = FRA_INF_01
						is_locked = yes
						regiments = {
							infantry = { x = 0 y = 0 }		
							infantry = { x = 0 y = 1 }		
							infantry = { x = 0 y = 2 }

							infantry = { x = 1 y = 0 }		
							infantry = { x = 1 y = 1 }
							infantry = { x = 1 y = 2 }		
						}
						support = {
							engineer = { x = 0 y = 0 }
							artillery = {x = 0 y = 1 }
						}
					}
					event_target:FRA_defender_state = { #set up defenders
						create_unit = {
							division = "name = \"West Africa Defense Force 1\" division_template = \"West Africa Defense Force\" start_experience_factor = 0.5"  
							owner = PREV
						}
					}
				}
			}
			event_target:FRA_attacker_state = {
				CONTROLLER = {
					start_border_war = {
						change_state_after_war = no
						attacker = {
							state = event_target:FRA_attacker_state
							num_provinces = 4
							on_win = FRA_west_africa_intervention.1
							on_lose = FRA_west_africa_intervention.2
							on_cancel = FRA_west_africa_intervention.3
						}
						defender = {
							state = event_target:FRA_defender_state
							num_provinces = 4
							on_win = FRA_west_africa_intervention.2
							on_lose = FRA_west_africa_intervention.1
							on_cancel = FRA_west_africa_intervention.3
						}
					}
				}
			}
		}
		fire_only_once = yes
		cost = 25
		ai_will_do = {
			factor = 0
		}
	}

	
	FRA_prepare_coup_in_madagascar = {
		available = { 
			NOT = {
				543 = {
					OR = {
						is_controlled_by = ROOT
						CONTROLLER = {
							is_subject_of = ROOT
						}
					}
				}
			}
			has_intelligence_agency = yes
		} 

		visible = { 
			has_completed_focus = FRA_intervention_in_madagascar
		}

		complete_effect = { 
			custom_effect_tooltip = FRA_prepare_coup_tt
			set_country_flag = FRA_coup_in_madagascar_flag
			
		}
		fire_only_once = yes
		cost = 25
		ai_will_do = {
			factor = 0
		}
	}
}
