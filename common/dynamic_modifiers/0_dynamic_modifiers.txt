#	Example:
# 
#	example_dynamic_modifier = {
#		icon = "GFX_idea_unknown" # optional, will show up in guis if icon is specified
#		enable = { always = yes } #optional, the modifier won't apply if not enabled
#		remove_trigger = { always = no } #optional, will remove the modifier if true
#
#		# list of modifiers
#		fuel_cost = 321
#		max_fuel = var_max_fuel # will be taken from a variable
#	}
#
#
#	In a script file:
#
#	effect = {
#		add_dynamic_modifier = {
#			modifier = example_dynamic_modifier
#			scope = GER # optional, if you specify this your dynamic modifier scoped to this scope (root is the effect scope)
#			days = 14 # optional, will be removed after this many days passes
#		}
#	}
#
#	can be added to countries, states or unit leaders
#	will only updated daily, unless forced by force_update_dynamic_modifier effect

###AXIS###
MEFO_bills_modifier_spirit = {
	enable = { always = yes }
	icon = GFX_idea_ger_mefo_bills
	remove_trigger = { has_war = yes }
	
	political_power_cost = ROOT.mefoppmodifier
	consumer_goods_factor = ROOT.mefoconsumergoodsmodifier
	production_speed_infrastructure_factor = ROOT.mefoinfrastructuremodifier
	production_speed_industrial_complex_factor = ROOT.mefocivilianmodifier
	production_speed_arms_factory_factor = ROOT.mefomilitarymodifier
	production_speed_dockyard_factor = ROOT.mefodockyardmodifier
	production_speed_naval_base_factor = ROOT.mefoportmodifier
	production_speed_coastal_bunker_factor = ROOT.mefocoastalmodifier
	production_speed_bunker_factor = ROOT.mefofortmodifier
	production_speed_air_base_factor = ROOT.mefoairportmodifier
	production_speed_anti_air_building_factor = ROOT.mefostateaamodifier
	production_speed_radar_station_factor = ROOT.meforadarmodifier
	production_speed_fuel_silo_factor = ROOT.mefosilomodifier
	production_speed_synthetic_refinery_factor = ROOT.meforefinerymodifier
}

GER_industrial_output_modifier = {
	enable = { always = yes }
	icon = GFX_idea_sour_loser
	remove_trigger = { always = no }
	
	drift_defence_factor = 0.5
}

ITA_industrial_output_modifier = {
	enable = { always = yes }
	icon = GFX_idea_victor_emmanuel
	remove_trigger = { always = no }
}

JAP_industrial_output_modifier = {
	enable = { always = yes }
	icon = GFX_idea_state_shintoism
	remove_trigger = { always = no }
}

HUN_industrial_output_modifier = {
	enable = { always = yes }
	icon = GFX_idea_HUN_treaty_of_triannon
	remove_trigger = { always = no }
}

ROM_industrial_output_modifier = {
	enable = { always = yes }
	icon = GFX_idea_ROM_the_sentinel_of_the_motherland 
	remove_trigger = { always = no }
}

BUL_industrial_output_modifier = {
	enable = { always = yes }
	icon = GFX_idea_ger_mefo_bills
	remove_trigger = { always = no }
}

## ALLIES ##
ENG_industrial_output_modifier = {
	enable = { always = yes }
	icon = GFX_idea_stiff_upper_lip
	remove_trigger = { always = no }
	
	drift_defence_factor = 0.5
	production_speed_buildings_factor = ROOT.generalconstructionspeedmodifier
	production_speed_infrastructure_factor = ROOT.infraconstructionspeedmodifier
	production_speed_synthetic_refinery_factor = ROOT.refineryconstructionspeedmodifier
	production_speed_naval_base_factor = ROOT.portconstructionspeedmodifier
	production_speed_radar_station_factor = ROOT.radarconstructionspeedmodifier
	production_speed_air_base_factor = ROOT.airfieldconstructionspeedmodifier
	production_speed_anti_air_building_factor = ROOT.antiairconstructionspeedmodifier
	production_speed_bunker_factor = ROOT.fortconstructionspeedmodifier
	production_speed_coastal_bunker_factor = ROOT.coastalfortconstructionspeedmodifier
	production_speed_rocket_site_factor = ROOT.rocketsiteconstructionspeedmodifier
	production_speed_nuclear_reactor_factor = ROOT.reactorconstructionspeedmodifier
	navy_refit_speed = ROOT.refitspeedmodifier
	navy_refit_ic_cost = ROOT.refiticmodifier
	transport_capacity = ROOT.transportcapacitymodifier
	industrial_capacity_factory = ROOT.militaryoutputmodifier
	industrial_capacity_dockyard = ROOT.dockyardoutputmodifier
	local_resources_factor = ROOT.localresourcesmodifier
	research_speed_factor = ROOT.localresearchmodifier
}

FRA_industrial_output_modifier = {
	enable = { always = yes }
	icon = GFX_idea_FRA_undividable
	remove_trigger = { always = no }
}

USA_industrial_output_modifier = {
	enable = { always = yes }
	icon = GFX_idea_man_five_year_plan_industry
	remove_trigger = { always = no }
} 
USA_great_depression_modifier = {
	enable = { always = yes }
	icon = GFX_idea_usa_great_depression
	remove_trigger = { 
		has_country_flag = USA_rural_electrification_act
		has_country_flag = USA_commodities_exchange_act
		has_country_flag = USA_robinson_patman_act
		has_country_flag = USA_flood_control_act
		has_country_flag = USA_walsh_healy_public_contracts_act
		has_country_flag = USA_agricultural_adjustment_act
		has_country_flag = USA_wheeler_lea_act
		has_country_flag = USA_natural_gas_act
		has_country_flag = USA_fair_labor_standards_act
		has_country_flag = USA_federal_housing_act
	}
	
	political_power_cost = ROOT.USAdepressionppmodifier
	consumer_goods_factor = ROOT.USAdepressionconsumermodifier
	conscription_factor = ROOT.USAdepressionconscriptionmodifier
	local_resources_factor = ROOT.USAdepressionlocalresourcesmodifier
} 

CAN_industrial_output_modifier = {
	enable = { always = yes }
	icon = GFX_idea_stiff_upper_lip
	remove_trigger = { always = no }
}

SAF_industrial_output_modifier = {
	enable = { always = yes }
	icon = GFX_idea_stiff_upper_lip
	remove_trigger = { always = no }
}

AST_industrial_output_modifier = {
	enable = { always = yes }
	icon = GFX_idea_stiff_upper_lip
	remove_trigger = { always = no }
	
}

RAJ_industrial_output_modifier = {
	enable = { always = yes }
	icon = GFX_idea_stiff_upper_lip
	remove_trigger = { always = no }
	
}

NZL_industrial_output_modifier = {
	enable = { always = yes }
	icon = GFX_idea_stiff_upper_lip
	remove_trigger = { always = no }
	
}

SOV_industrial_output_modifier = {
	enable = { always = yes }
	icon = GFX_idea_home_of_revolution
	remove_trigger = { always = no }
	
}

###Great Patriotic War###
great_patriotic_modifier_spirit = {
	enable = { 
		SOV = { has_war = yes }
	}
	remove_trigger = {
		has_completed_focus = SOV_defense_of_moscow
	}
	icon = GFX_goal_sov_great_patriotic_war
	
	army_org_factor = SOV.great_patriotic_war_land_buff
	army_morale_factor = SOV.great_patriotic_war_land_buff
	
	air_superiority_efficiency = SOV.great_patriotic_war_air_buff
	air_escort_efficiency = SOV.great_patriotic_war_air_buff
	air_intercept_efficiency = SOV.great_patriotic_war_air_buff
}

sabotaged_resources = {
	remove_trigger = { 
		has_resistance = no
	}
	
	icon = GFX_modifiers_sabotaged_resource
	
	temporary_state_resource_oil = sabotaged_oil
	temporary_state_resource_aluminium = sabotaged_aluminium
	temporary_state_resource_rubber = sabotaged_rubber
	temporary_state_resource_tungsten = sabotaged_tungsten
	temporary_state_resource_steel = sabotaged_steel
	temporary_state_resource_chromium = sabotaged_chromium
}

autonomous_state = {
	enable = { always = yes }

	icon = GFX_modifiers_sabotaged_resource
	
	recruitable_population_factor = -0.5
	local_building_slots_factor = -0.25
	state_resources_factor = -0.25
	state_production_speed_buildings_factor = -0.25
}

semi_autonomous_state = {
	enable = { always = yes }

	icon = GFX_modifiers_sabotaged_resource
	
	local_building_slots_factor = -0.25
	state_resources_factor = -0.25
	state_production_speed_buildings_factor = -0.25
}